﻿using System.Collections.Generic;
using UnityEngine;

namespace Train
{
    public class RailwayInfo : MonoBehaviour
    {
        [SerializeField] private Transform checkpointsParent = null;        
        [Range(2, 100)]
        [SerializeField] private int interpolationSteps = 4;
        [SerializeField] private bool isDebug = false;
   
        [HideInInspector][SerializeField] private List<Vector3> checkpointsBase = new List<Vector3>();
        [HideInInspector][SerializeField] private List<Vector3> checkpoints = new List<Vector3>();

        public Transform CheckpointsParent => checkpointsParent;
        public List<Vector3> Checkpoints => checkpoints;
        public List<Vector3> CheckpointsBase => checkpointsBase;
        public int InterpolationSteps => interpolationSteps;

        private void OnDrawGizmos()
        {
            if (!isDebug)
                return;

            Gizmos.color = Color.blue;
            foreach (var it in Checkpoints)
            {
                Gizmos.DrawSphere(it, 1);
            }
        }
    }
}
