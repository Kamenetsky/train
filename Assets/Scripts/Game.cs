﻿using Train.UI;
using UnityEngine;

namespace Train
{
    public class Game : MonoBehaviour
    {
        public static Game instance;

        private enum TrainState
        {
            Stop = 0,
            Accelerate = 10,
            Move = 20,
            Braking = 30
        }

        [SerializeField] private TrainView train = null;
        [SerializeField] private RailwayInfo railway = null;

        [SerializeField] private UIManager ui = null;

        private const float checkpointThreshold = 0.01f;
        private const float acceleration = 2f;
        private const float breaking = 3f;
        private const float maxMoveSpeed = 15f;

        private float moveSpeed = 0f;
        private float rotationSpeed = 5f;
        private int currentCheckpoint = 0;
        
        private TrainState currentState = TrainState.Stop;
        private bool isNearStation = false;
        private int scores = 0;
        private float time = 300f;
        private bool isStarted = false;

        private void Awake()
        {
            instance = this;
            TrainView.OnStationInteract += OnStationInteract;
        }

        private void OnDestroy()
        {
            TrainView.OnStationInteract -= OnStationInteract;
        }

        private void Start()
        {
            StartGame();
        }

        public void StartGame()
        {
            currentCheckpoint = 0;
            train.transform.position = railway.Checkpoints[currentCheckpoint];
            train.transform.LookAt(railway.Checkpoints[currentCheckpoint + 1]);
            currentState = TrainState.Stop;
            StartMove();
            isNearStation = false;
            scores = 0;
            time = 300f;
            ui.ShowTime(time);
            ui.ShowScores(scores);
            ui.ShowCargoButton(false);
            isStarted = true;
        }

        private void Update()
        {
            if (!isStarted)
                return;

            time -= Time.deltaTime;

            if (time <= 0)
            {
                isStarted = false;
                ui.ShowResults(scores);
                train.PlayMoveSound(false);
                return;
            }
            
            ui.ShowTime(time);
            
            if (Vector3.Distance(train.transform.position, railway.Checkpoints[currentCheckpoint]) <= checkpointThreshold)
            {
                currentCheckpoint++;               
                if (currentCheckpoint >= railway.Checkpoints.Count)
                {
                    currentCheckpoint = 0;
                }
            }

            ProcessTrainState();
            
            Vector3 nextPos = railway.Checkpoints[currentCheckpoint];
            var horizontalVector = new Vector3(nextPos.x - train.transform.position.x, 0f, nextPos.z - train.transform.position.z).normalized;
            train.transform.rotation = Quaternion.Slerp(train.transform.rotation, Quaternion.LookRotation(horizontalVector), rotationSpeed * Time.deltaTime);
            train.transform.position = Vector3.MoveTowards(train.transform.position, railway.Checkpoints[currentCheckpoint], Time.deltaTime * moveSpeed);
            train.SetRotationSpeed(moveSpeed);
        }

        public void StartMove()
        {
            if (currentState != TrainState.Stop)
                return;

            currentState = TrainState.Accelerate;
            ui.ShowMoveButton(false);
            ui.ShowStopButton(false);
        }

        public void StopMove()
        {
            if (currentState != TrainState.Move)
                return;
            currentState = TrainState.Braking;
            ui.ShowMoveButton(false);
            ui.ShowStopButton(false);
        }

        public void CollectCargo()
        {
            scores += 1;
            ui.ShowScores(scores);
            ui.ShowCargoButton(false);
        }

        public void TrainSignal()
        {
            train.PlaySignal();
        }
        
        private void ProcessTrainState()
        {
            if (currentState == TrainState.Accelerate)
            {
                moveSpeed += acceleration * Time.deltaTime;
                train.PlayMoveSound(true);
                if (moveSpeed >= maxMoveSpeed)
                {
                    moveSpeed = maxMoveSpeed;
                    currentState = TrainState.Move;
                    ui.ShowStopButton(true);
                }
            }
            else if (currentState == TrainState.Braking)
            {
                moveSpeed -= breaking * Time.deltaTime;
                if (moveSpeed <= 0f)
                {
                    train.PlayMoveSound(false);
                    moveSpeed = 0f;
                    currentState = TrainState.Stop;
                    ui.ShowMoveButton(true);
                    ui.ShowCargoButton(isNearStation);
                }
            }
        }

        private void OnStationInteract(bool isEnter)
        {
            isNearStation = isEnter;
        }
    }
}
