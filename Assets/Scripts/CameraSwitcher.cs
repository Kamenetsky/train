﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Train
{
    public class CameraSwitcher : MonoBehaviour
    {
        public static CameraSwitcher instance = null;
        
        [SerializeField] private GameObject camera0 = null;
        [SerializeField] private GameObject camera1 = null;

        private void Awake()
        {
            instance = this;
        }

        public void SwitchCamera()
        {
            if (camera0.activeSelf)
            {
                camera0.SetActive(false);
                camera1.SetActive(true);
            }
            else
            {
                camera0.SetActive(true);
                camera1.SetActive(false);
            }
        }
    }
}
