﻿using Train.Utils;
using UnityEditor;
using UnityEngine;

namespace Train.Editor
{
    [CustomEditor(typeof(RailwayInfo))]
    public class RailwayInfoEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Create Path"))
            {
                RailwayInfo info = (RailwayInfo)target;
                GetCheckpoints(info);
                Interpolate(info);
            }
        }

        private void GetCheckpoints(RailwayInfo info)
        {
            info.CheckpointsBase.Clear();
            
            for (int i = 0; i < info.CheckpointsParent.childCount; ++i)
            {
                info.CheckpointsBase.Add(info.CheckpointsParent.GetChild(i).position);
            }
        }
        
        private void Interpolate(RailwayInfo info)
        {
            if (info.CheckpointsBase.Count < 4)
            {
                Debug.LogWarning("Can not find control checkpoints!");
                return;
            }
            
            info.Checkpoints.Clear();

            int amount = info.CheckpointsBase.Count * info.InterpolationSteps;
            for (int i = 0; i < amount; i++)
            {
                info.Checkpoints.Add(Bezier.GetPoint4(Bezier.PathControlPoints(info.CheckpointsBase.ToArray()), (float)i / amount));
            }
        }
    }
}
