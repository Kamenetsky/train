﻿using UnityEngine;
using UnityEngine.UI;

namespace Train.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private GameObject moveButton = null;
        [SerializeField] private GameObject stopButton = null;
        [SerializeField] private GameObject cargoButton = null;
        [SerializeField] private GameObject resultPanel = null;

        [SerializeField] private Text resultScoresText = null;
        [SerializeField] private Text scoresText = null;
        [SerializeField] private Text timeText = null;

        private void Start()
        {
            resultPanel.SetActive(false);
        }
        
        public void ShowMoveButton(bool show)
        {
            moveButton.SetActive(show);
        }

        public void OnStartMove()
        {
            Game.instance.StartMove();
        }
        
        public void ShowStopButton(bool show)
        {
            stopButton.SetActive(show);
        }

        public void OnStopMove()
        {
            Game.instance.StopMove();
        }

        public void ShowCargoButton(bool show)
        {
            cargoButton.SetActive(show);
        }

        public void OnCollectCargo()
        {
            Game.instance.CollectCargo();
        }
        
        public void OnRestart()
        {
            resultPanel.SetActive(false);
            Game.instance.StartGame();
        }

        public void ShowScores(int scores)
        {
            scoresText.text = "Scores: " + scores.ToString();
        }
        
        public void ShowTime(float time)
        {
            timeText.text = TimeToStringMMSS(time);
        }

        public void ShowResults(int scores)
        {
            resultPanel.SetActive(true);
            resultScoresText.text = "Total scores: " + scores.ToString();
        }

        public void OnSignal()
        {
            Game.instance.TrainSignal();
        }

        public void OnSwitchCamera()
        {
            CameraSwitcher.instance.SwitchCamera();
        }
        
        public static string TimeToStringMMSS(float time)
        {
            string minutes = Mathf.Floor(time / 60).ToString("00");
            string seconds = (time % 60).ToString("00");
            return string.Format("{0}:{1}", minutes, seconds);
        }

    }
}
