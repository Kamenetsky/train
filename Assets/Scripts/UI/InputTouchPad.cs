﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

namespace Train.UI
{
    public class InputTouchPad : MonoBehaviour, IDragHandler, IEndDragHandler, IInitializePotentialDragHandler
    {
        public string horizontalAxisName = "Horizontal";
        public string verticalAxisName = "Vertical";
        public float Xsensitivity = 1f;
        public float Ysensitivity = 1f;
        
        private PointerEventData pointerData;
        private Vector3 lastPosition;
        private float maxScreenSide;
        private int currentPointerID = -1;
        
        private CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; 
        private CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; 
                
        void OnEnable()
        {
            CreateVirtualAxes();
        }
        
        void OnDisable()
        {
            if (CrossPlatformInputManager.AxisExists(horizontalAxisName))
                CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);

            if (CrossPlatformInputManager.AxisExists(verticalAxisName))
                CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);
        }
        
        void UpdateVirtualAxes(Vector3 value)
        {
            value = value.normalized;
            m_HorizontalVirtualAxis.Update(value.x);
            m_VerticalVirtualAxis.Update(value.y);
        }
        
        void CreateVirtualAxes()
        {
            m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
            
            m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
        }
        
        public void OnDrag(PointerEventData eventData)
        {
            if (pointerData == null)
            {
                pointerData = eventData;
                lastPosition = pointerData.position;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData == pointerData)
            {
                pointerData = null;
                UpdateVirtualAxes(Vector3.zero);
            }
        }

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            eventData.useDragThreshold = true;
        }

        private void Awake()
        {
            maxScreenSide = Mathf.Max(Screen.width, Screen.height);
        }

        private void Update()
        {
            if (pointerData == null)
                return;

            Vector3 input = pointerData.position;
            Vector3 delta = (input - lastPosition) / maxScreenSide;
            lastPosition = input;
            UpdateVirtualAxes(new Vector3(delta.x * Xsensitivity, delta.y * Ysensitivity, 0));
        }
    }
}
