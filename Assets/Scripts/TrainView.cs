﻿using System;
using UnityEngine;

namespace Train
{
    public class TrainView : MonoBehaviour
    {
        public static Action<bool> OnStationInteract = delegate { };
        
        [SerializeField] private Transform[] wheels = null;
        [SerializeField] private AudioSource signalSource = null;
        [SerializeField] private AudioSource moveSource = null;

        private const float rotKoef = 100f;
        private float wheelsRotSpeed = 0f;
        
        public void SetRotationSpeed(float speed)
        {
            wheelsRotSpeed = speed * rotKoef;
        }

        public void PlaySignal()
        {
            signalSource.Play();
        }

        public void PlayMoveSound(bool play)
        {
            if (play && !moveSource.isPlaying)
            {
                moveSource.Play();
                return;
            }
            
            if (!play && moveSource.isPlaying)
            {
                moveSource.Stop();
            }
        }

        private void Update()
        {
            if (Mathf.Abs(wheelsRotSpeed) <= float.Epsilon)
                return;
            
            foreach (var wheel in wheels)
            {
                wheel.Rotate(Vector3.right, Time.deltaTime * wheelsRotSpeed);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("station"))
            {
                OnStationInteract(true);
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("station"))
            {
                OnStationInteract(false);
            }
        }
    }
}
